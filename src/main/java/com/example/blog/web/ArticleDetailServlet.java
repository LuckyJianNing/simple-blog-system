package com.example.blog.web;

import com.example.blog.pojo.Article;
import com.example.blog.service.ArticleService;
import com.example.blog.service.impl.ArticleServiceImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/articleDetail")
public class ArticleDetailServlet extends AbstractBaseServlet{

    private ArticleService articleService = new ArticleServiceImpl();
    //get方法
    @Override
    protected Object process(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        String id = req.getParameter("id");
        Article article = articleService.queryById(Integer.parseInt(id));

        System.out.println("使用过detail了");
        return article;
    }
}
