package com.example.blog.web;

import com.example.blog.pojo.Article;
import com.example.blog.service.ArticleService;
import com.example.blog.service.impl.ArticleServiceImpl;
import com.example.blog.util.JSONUtil;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
@WebServlet("/articleUpdate")
public class ArticleUpdateServlet extends AbstractBaseServlet{

    private ArticleService articleService = new ArticleServiceImpl();

    @Override
    protected Object process(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        InputStream in = req.getInputStream();
        //反序列化
        Article article = JSONUtil.deserialize(in,Article.class);
        int num = articleService.updateArt(article);
        return num;
    }
}
