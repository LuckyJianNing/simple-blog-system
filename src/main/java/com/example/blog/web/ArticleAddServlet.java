package com.example.blog.web;

import com.example.blog.pojo.Article;
import com.example.blog.pojo.User;
import com.example.blog.service.ArticleService;
import com.example.blog.service.impl.ArticleServiceImpl;
import com.example.blog.util.JSONUtil;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.InputStream;
@WebServlet("/articleAdd")
public class ArticleAddServlet extends AbstractBaseServlet{

    private ArticleService articleService = new ArticleServiceImpl();

    @Override
    protected Object process(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        HttpSession session = req.getSession(false);
        User user = (User) session.getAttribute("user");
        //请求的数据类型是JSON，所以要反序列化
        InputStream in = req.getInputStream();
        Article article = (Article) JSONUtil.deserialize(in,Article.class);
        article.setUser_id(user.getId());
        articleService.insertArt(article);
        return null;


    }
}
