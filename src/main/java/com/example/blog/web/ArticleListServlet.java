package com.example.blog.web;

import com.example.blog.Exception.AppException;
import com.example.blog.pojo.Article;
import com.example.blog.pojo.User;
import com.example.blog.service.ArticleService;
import com.example.blog.service.impl.ArticleServiceImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
@WebServlet("/articleList")
public class ArticleListServlet extends AbstractBaseServlet{

    private ArticleService articleService = new ArticleServiceImpl();
    //get方法
    @Override
    protected Object process(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        HttpSession session = req.getSession(false);
        if(session == null) {
            throw new AppException("ART002", "用户没有登录不允许访问");
        }
        User user = (User) session.getAttribute("user");
        if(user == null) {
            throw new AppException("ART003", "会话异常，请重新登录");
        }
        List<Article> articles = articleService.queryByUserId(user.getId());
        return articles;
    }
}
