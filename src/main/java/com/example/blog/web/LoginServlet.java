package com.example.blog.web;

import com.example.blog.Exception.AppException;
import com.example.blog.dao.UserDao;
import com.example.blog.pojo.User;
import com.example.blog.service.UserService;
import com.example.blog.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
@WebServlet("/login")
public class LoginServlet extends AbstractBaseServlet {

    private UserService userService = new UserServiceImpl();

    //post方法
    @Override
    protected Object process(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        User user = userService.loginUser(new User(null,username,password,false,null));


        if(!userService.existsUsername(username)){
            throw new AppException("LOG002","用户不存在");
        }

        if(user == null){
            throw new AppException("LOG003","用户名或密码错误");
        }


        //不等于空，说明登陆成功
        HttpSession session  = req.getSession();
        session.setAttribute("user",user);

        return null;
    }
}
