package com.example.blog.web;

import com.example.blog.service.ArticleService;
import com.example.blog.service.impl.ArticleServiceImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/articleDelete")
public class ArticleDeleteServlet extends AbstractBaseServlet{

    private ArticleService articleService = new ArticleServiceImpl();

    @Override
    protected Object process(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        String ids = req.getParameter("ids");

        int num = articleService.deleteArt(ids.split(","));

        return num;
    }
}
