package com.example.blog.service;

import com.example.blog.pojo.User;

public interface UserService {

    /**
     * 注册用户
     * @param user
     * @return
     */
    public void registerUser(User user);

    /**
     *  用户登录
     * @param user
     * @return
     */
    public User loginUser(User user);

    /**
     * 判断username是否存在
     * @param username
     * @return
     */
    public Boolean existsUsername(String username);


}
