package com.example.blog.service;

import com.example.blog.pojo.Article;

import java.util.List;

public interface ArticleService {

    /**
     * 新建博客
     * @param article
     */
    public void insertArt(Article article);

    /**
     * 删除博客
     * @return
     */
    public int deleteArt(String[] split);

    /**
     * 修改博客
     * @param article
     */
    public int updateArt(Article article);

    /**
     * 通过文章id查找文章
     * @param id
     * @return
     */
    public Article queryById(Integer id);

    /**
     * 通过用户id查找用户文章
     * @return
     */
    public List<Article> queryByUserId(Integer user_id);

}
