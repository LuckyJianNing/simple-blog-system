package com.example.blog.service.impl;

import com.example.blog.dao.UserDao;
import com.example.blog.dao.impl.UserDaoImpl;
import com.example.blog.pojo.User;
import com.example.blog.service.UserService;

public class UserServiceImpl implements UserService {

    private UserDao userDao = new UserDaoImpl();

    @Override
    public void registerUser(User user) {
        userDao.saveUser(user);
    }

    @Override
    public User loginUser(User user) {
        return userDao.queryUserByUsernameAndPassword(user.getUsername(),user.getPassword());
    }

    @Override
    public Boolean existsUsername(String username) {
        User user = userDao.queryUserByUsername(username);
        if(user == null){
            //说明该用户名可用，不存在重复用户名
            return false;
        }else{
            return true;
        }
    }
}
