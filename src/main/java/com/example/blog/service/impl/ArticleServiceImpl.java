package com.example.blog.service.impl;

import com.example.blog.dao.ArticleDao;
import com.example.blog.dao.impl.ArticleDaoImpl;
import com.example.blog.pojo.Article;
import com.example.blog.pojo.User;
import com.example.blog.service.ArticleService;

import java.util.List;

public class ArticleServiceImpl implements ArticleService {

    private ArticleDao articleDao = new ArticleDaoImpl();

    @Override
    public void insertArt(Article article) {
        articleDao.insertArticleDao(article);
    }

    @Override
    public int deleteArt(String[] split) {
        return articleDao.deleteArticle(split);
    }

    @Override
    public int updateArt(Article article) {
        return articleDao.updateArticle(article);
    }

    @Override
    public Article queryById(Integer id) {
        return articleDao.queryByArticleId(id);
    }

    @Override
    public List<Article> queryByUserId(Integer user_id) {
        return articleDao.queryByUser_id(user_id);
    }


    public static void main(String[] args) {
        ArticleService articleService = new ArticleServiceImpl();
        User user = new User();
        user.setId(1);
        user.setUsername("a");
        user.setPassword("1");
        List<Article> articles = articleService.queryByUserId(user.getId());
        for(Article a : articles){
            System.out.println(a);
        }
    }
}
