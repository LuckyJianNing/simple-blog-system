package com.example.blog.pojo;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private Integer id;
    private String username;
    private String password;
    private boolean sex;
    private java.sql.Date birthday;


    /**
     * lombok Boolean类型通过is来获取
     * @param args
     */
    public static void main(String[] args) {
        User u = new User();
        u.setSex(true);
        System.out.println(u.isSex());
    }
}


