package com.example.blog.pojo;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Article {

    private Integer id;
    private String title;
    private String content;
    private Integer view_count;
    private Integer user_id;
    private java.sql.Timestamp create_time;
}



class Demo{
    public static void main(String[] args) {
        Article a = new Article();
        a.setId(1);
        System.out.println(a.getId());
    }
}
