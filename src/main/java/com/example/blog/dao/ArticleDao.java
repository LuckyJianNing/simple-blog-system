package com.example.blog.dao;

import com.example.blog.dao.BaseDao;
import com.example.blog.pojo.Article;

import java.util.List;

public interface ArticleDao{

    /**
     * 创建新文章
     * @return
     */
    public int insertArticleDao(Article article);

    /**
     * 通过用户id来遍历出该用户的所有博客
     * @param user_id
     * @return
     */
    public List<Article> queryByUser_id(Integer user_id);

    /**
     * 通过文章Id查找
     * @param id
     * @return
     */
    public Article queryByArticleId(Integer id);

    /**
     * 修改文章
     * @param article
     * @return
     */
    public int updateArticle(Article article);

    /**
     * 删除文章
     * @param split 要删除的id用String[]数组来存储
     * @return
     */
    public int deleteArticle(String[] split);
}
