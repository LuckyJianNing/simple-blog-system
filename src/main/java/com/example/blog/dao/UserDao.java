package com.example.blog.dao;

import com.example.blog.pojo.User;

public interface UserDao {
    /**
     *  注册的时候判断用户名是否可用
     * @param username
     * @return
     */
    public User queryUserByUsername(String username);

    /**
     * 登陆的时候判断用户名和密码是否正确
     * @param username
     * @param password
     * @return
     */
    public User queryUserByUsernameAndPassword(String username,String password);

    /**
     * 注册成功保存用户信息
     * @param user
     */
    public int saveUser(User user);


}
