package com.example.blog.dao.impl;

import com.example.blog.dao.BaseDao;
import com.example.blog.dao.UserDao;
import com.example.blog.pojo.User;

public class UserDaoImpl extends BaseDao implements UserDao {

    @Override
    public User queryUserByUsername(String username) {
        String sql = "select * from user where username = ?";
        return queryForOne(User.class,sql,username);
    }

    @Override
    public User queryUserByUsernameAndPassword(String username, String password) {
        String sql = "select * from user where username = ? and password = ?";
        return queryForOne(User.class,sql,username,password);
    }

    @Override
    public int saveUser(User user) {
        String sql = "insert into user (username, password, sex, birthday) value(?,?,?,?)";
        return update(sql,user.getUsername(),user.getPassword(),user.isSex(),user.getBirthday());
    }
}
