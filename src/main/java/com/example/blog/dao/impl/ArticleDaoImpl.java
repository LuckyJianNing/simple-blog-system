package com.example.blog.dao.impl;

import com.example.blog.dao.ArticleDao;
import com.example.blog.dao.BaseDao;
import com.example.blog.pojo.Article;

import java.util.List;

public class ArticleDaoImpl extends BaseDao implements ArticleDao {


    @Override
    public int insertArticleDao(Article article) {
        String sql = "insert into article(title,content,user_id) values (?,?,?)";
        return update(sql,article.getTitle(),article.getContent(),article.getUser_id());
    }

    @Override
    public List<Article> queryByUser_id(Integer user_id) {
        String sql = "select id,title,content from article where user_id=?";
        return queryForList(Article.class,sql,user_id);
    }

    @Override
    public Article queryByArticleId(Integer id) {
        String sql = "select id,title,content from article where id=?";
        return queryForOne(Article.class,sql,id);
    }

    @Override
    public int updateArticle(Article article) {
        String sql ="update article set title=?,content=? where id=?";
        return update(sql,article.getTitle(),article.getContent(),article.getUser_id());
    }

    @Override
    public int deleteArticle(String[] split) {
        StringBuilder sql =new StringBuilder( "delete from article where id in (");
        sql.append("?");
        for(int i=1;i<split.length;i++){
            sql.append(",");
            sql.append("?");
        }
        sql.append(")");
        return update(sql.toString());
    }
}
