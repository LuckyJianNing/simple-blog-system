package com.example.blog.Exception;

public class AppException extends RuntimeException {
    //给前端返回的json字符串中，保存错误码
    private String code;
    public AppException(String code,String message) {
        //     super(message);
        //    this.code = code;
        this(code,message,null);
    }

    public AppException(String code,String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public String getCode() {
        return code;
    }


}