<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="GBK">
    <title>注册页面</title>
    <script type="text/javascript" src="../static/jquery/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="../js/register.js"></script>
</head>

<body>
<div style="width:100%;text-align:center">
<h2>用户注册</h2>
<form id="register_form" method="post" action="../register" enctype="application/x-www-form-urlencoded">
    <label>用户名称：</label>
    <input id="username" type="text" name="username" placeholder="请输入用户名" required="required"><br><br>
    <label>用户密码：</label>
    <input id="password" type="password" name="password" placeholder="请输入密码"required="required"><br><br>
    <label>确认密码：</label>
    <input id="repwd" type="password" name="repwd" placeholder="再次输入密码"required="required"><br><br>
    <label>您的性别：</label>
    <select id="sex" name="sex" required="required">
        <option value ="true">男</option>
        <option value ="false">女</option>
    </select>
    <br><br>
    <label>出生年月：</label>
    <input id="birthday" type="date" name="birthday" required="required"placeholder="请选择出生年月"><br><br>
    <label>验证码：</label>
    <input id="code" name="code" type="text" required="required" style="width: 50px"/>
    <img src="/blog/kaptcha.jpg" alt=" " style="height: 28px"><br><br>
    <input type="submit" value="注册" id="sub_btn">


</form>
</div>
</body>

<script type="text/javascript">
    $(function () {
        // 给注册绑定单击事件
        $("#sub_btn").click(function () {
            $(function () {
                // 验证确认密码：和密码相同
                //1 获取确认密码内容

                var passwordText = $("#password").val();
                var repwdText = $("#repwd").val();
                //2 和密码相比较
                if (repwdText != passwordText) {
                    //3 提示用户
                    alert("确认密码和密码不一致！")

                    return false;
                }

            });
        });
    });
</script>
</html>

